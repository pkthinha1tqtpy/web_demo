import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';
import TaskList from './TaskList';

ReactDOM.render(<TaskList />, document.getElementById('root'));
