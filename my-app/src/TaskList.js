import React, { Component } from 'react';
import TodoList from './TodoList';
import axios from "axios";
import ReactPlayer from "react-player";
import AddTask from './AddTask';

class TaskList extends Component {
    constructor(props) {
        super(props)
        
        this.state = {
            tasks: [],
            showAddForm: false,
            link:'',
            state_link:0,
            view_tasks:[],
            url:"",
            activeItem: {
                stt: "",
                name: "",
                point: false,
              },
              leng:0
            
        }
    }
    createItem = () => {
        const item = { stt: "", name: "", point: false };
    
        this.setState({ activeItem: item});
      };
    componentDidMount() {
        this.refreshList();
      }
    
    refreshList = () => {
        axios
          .get("/api/todos/")
          .then((res) => this.setState({ tasks: res.data }))
          .catch((err) => console.log(err));
          this.state.leng= this.state.tasks.length
          this.closeForm()
      };

    handleSubmit = (item) => {
        // this.toggle();
    
        if (item.stt) {
          axios
            .put(`/api/todos/${item.stt}/`, item)
            .then((res) => this.refreshList());
          return;
        }
        axios
          .post("/api/todos/", item)
          .then((res) => this.refreshList());
      };
    setStatus = () => {
        this.setState({
            showAddForm: true
        })
    }
    closeForm = () => {
        this.setState({
            showAddForm: false,
            showEditForm: false
        })
        
    }
    addTask = (stt,name,point) => {
        this.state.tasks.push({stt,name,point})
        this.forceUpdate()
    }
    addViewTask = (stt,name,point) => {
        this.state.view_tasks.push({stt,name,point})
        // this.forceUpdate()
    }
    currentask=()=>{
        this.setState({
            state_link: 0
        })
    }
    goodtask=()=>{
        this.setState({
            state_link: 1
        })
    }
    badtask=()=>{
        this.setState({
            state_link: 2
        })
    }
    all_video=()=>{
        if(this.state.state_link==0){
            this.state.view_tasks=this.state.tasks
        }
        else if(this.state.state_link==1){
            this.state.view_tasks=[]
            for(let i=0;i<this.state.tasks.length;){
                
                if (this.state.tasks[i].point==true){
                    // this.state.view_tasks.this.state.tasks[i]]
                    this.addViewTask(this.state.tasks[i].stt,this.state.tasks[i].name,this.state.tasks[i].point)
                   
                }
                i++
                
            }

        }
        else if(this.state.state_link==2){
            this.state.view_tasks=[]
            
            for(let i=0;i<this.state.tasks.length;){
                if (this.state.tasks[i].point==false){
                    // this.state.view_tasks+=[this.state.tasks[i]]
                    this.addViewTask(this.state.tasks[i].stt,this.state.tasks[i].name,this.state.tasks[i].point)
                   
                }
                i++
            }

        }
    }
    isChangedValue = (v) => {
        this.setState({
            link: v.target.value
        })
    }
    findindex=(stt)=>{
        var tasks=this.state.tasks;
        tasks.forEach((task,index)=>{
            if(task.stt==stt){
                return index;
            }
        });
        return -1;
    }
    deleteRow=(stt)=>{
        var task=this.state.tasks;
        var index=this.findindex(stt);
        if(index !== -1){
        task.splice(index,1);
        this.setState({
            tasks: task,
        });}    
    }
    setUrl=(name)=>{
        this.setState({
            url:name
        })
    }
    renderTableData() {
        {this.all_video()}
        return this.state.view_tasks.map((view_tasks, index) => {
                
          const { stt, name, point } = view_tasks
          return (
            <tr key={stt}>
              <td>{stt}</td>
              
              <td>{name}</td>
              <td>{point==true? 'good':'bad'}</td>
              <button onClick={() => this.setUrl(name)}>Hiển thị video</button>
              <button onClick={() => this.deleteRow(stt)}>Xóa video</button>
           
            </tr>
          )
        })
      }
    
      renderTableHeader() {
        
        const header = Object.keys(this.state.tasks[0]).map((key, index) => <th key={index}>{key.toUpperCase()}</th>)
        
        header.push(<th>ACTION</th>);
        return header;
      }
    render() {
        if (this.state.showAddForm === true) {
            return (
                <AddTask item={this.activeItem} refreshList={this.refreshList} createItem={this.createItem} leng={this.state.tasks.length} addTask={this.addTask} closeForm={this.closeForm} />
            )
        } else {
            if( this.state.tasks.length==0 ){
                return (
                    <div className="container">
                        <br />
                        <br />
                        
                        <button type="button" className="btn btn-outline-primary" onClick={this.setStatus} >Add Task</button>
                        <h2>List Task</h2>
                        <h1>Bạn chưa có video nào hết á</h1>
                        
                    </div>
                );
                
            }
            else{
            return (
                <div className="container">
                    <br />
                    <br />
                   
                    <button type="button" className="btn btn-outline-primary" onClick={this.setStatus} >Add Task</button>
                    <h2>List Task</h2>
                    <button type="button" className="btn btn-outline-primary" onClick={this.currentask} >Toàn bộ video</button>
            <button type="button" className="btn btn-outline-primary" onClick={this.goodtask} >Good video</button>
            <button type="button" className="btn btn-outline-primary" onClick={this.badtask} >Bad video</button>
                    
                    <table className="table table-striped">
                        
                    <tbody>
            <tr>{this.renderTableHeader()}</tr>
            {this.renderTableData()}
          </tbody>
                    </table>
           
            {/* <h3>Embed YouTube video - <a href="https://www.cluemediator.com">Clue Mediator</a></h3> */}
      <ReactPlayer
        url={this.state.url}
      />
                </div>
                
                
            );
            }
        }
    }
}

export default TaskList;