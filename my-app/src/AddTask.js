import React, { Component } from 'react';
import TaskList from './TaskList';
import axios from "axios";
import {Input} from "reactstrap"

class AddTask extends Component {
    constructor(props) {
        super(props)
        this.state = {
            
            activeitem:this.props.item,
          
           
        
        }
    }
    // createItem = () => {
    //     const item = { stt: this.state.leng, name: this.state.name, point: this.state.leng%2==0?true:false };
    
    //     this.setState({
    //          activeItem: item
             
    //          });
       
    //   };
    linkList = () => {
        this.props.closeForm()
    }
    handleAddTask = () => {
        
        
        this.handleSubmit(this.state.activeitem);
        
    }
    // refreshList = () => {
    //     axios
    //       .get("/api/todos/")
    //       .then((res) => this.setState({ this.tasks: res.data }))
    //       .catch((err) => console.log(err));
    //   };

    handleSubmit = (item) => {        
          axios
            .post("/api/todos/", item)
            .then((res) => this.props.refreshList());
      
          
        
      
      };
    isChangedName = (e) => {
        
        const value= e.target.value
        
        // console.log(stt)
        const activeItem = {stt: this.props.leng+1,name: value,point:this.props.leng%2==0?true:false };
        
        this.setState({
            activeitem:activeItem
        })
    }
    
    render() {
        if (this.state.showTaskList == true) {
            return (
                <TaskList />
            )
        } else {
            return (
                <React.Fragment>
                    <div className="container">
                        <h2>Add New Task</h2>
                        <div className="form-group">
                            <label>Link</label>
                            <Input 
                            type="text"
                            id="todo_name" 
                            name="name"
                            // value={this.state.activeitem.name}
                            className="form-control" 
                            placeholder="Enter link of you" 
                            onChange={this.isChangedName} />
                        </div>
                        
                        

                        <button type="submit" style={{ marginRight: 5 + 'px' }} className="btn btn-default" onClick={this.handleAddTask}>Add</button>
                        <button type="button" className="btn btn-default" onClick={this.linkList}>Back</button>
                        <h2>{this.activeItem}</h2>
                    </div>
                </React.Fragment>
            );
        }
    }
}

export default AddTask;