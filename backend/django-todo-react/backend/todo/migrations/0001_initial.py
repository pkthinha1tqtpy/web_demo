# Generated by Django 3.2.6 on 2021-08-12 15:09

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Todo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('stt', models.CharField(max_length=120)),
                ('name', models.TextField()),
                ('point', models.BooleanField(default=False)),
            ],
        ),
    ]
