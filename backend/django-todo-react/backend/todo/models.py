from django.db import models

# Create your models here.

class Todo(models.Model):
    stt = models.CharField(max_length=120)
    name = models.TextField()
    point = models.BooleanField(default=False)

    def _str_(self):
        return self.stt